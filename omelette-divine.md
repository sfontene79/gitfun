# Omelette divine

## Préparation

Temps total: 15 min

| Étape       | Durée |
| ------------| ----- |
| Préparation | 5mn   |
| Repos       | -     |
| Cuisson     | 10mn  |

## Ingrédients (2)

Pour 4 personnes :

- 7 œuf
- 50g beurre

## Recette

1. Battez les oeufs à la fourchette, salez et poivrez.

2. Faites chauffer le beurre, versez-en un peu dans les oeufs et mélangez. Versez les oeufs
dans la poêle à feu vif, baissez le feu et laissez cuire doucement en ramenant les bords de
l'omelette au centre au fur et à mesure qu'ils prennent.

3. Secouez un peu la poêle pour éviter que l'omelette n'attache, vérifiez la texture
baveuse ou bien prise.

4. Pliez l'omelette en deux et servez.
