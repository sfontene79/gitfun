test_titre() {
  if  head -1 $1 | grep -q "^#"; then
    echo OK
  else
    echo ERREUR: titre manquant
  fi
}

test_chapitres() {
  if [[ $(grep "^##" $1 | wc -l) != 3 ]]; then
    echo ERREUR: chapitres incorrects
  elif [[ $(grep "^##" $1 | head -1) != "## Préparation" ]]; then
    echo ERREUR: chapitre "Préparation" incorrect
  elif [[ $(grep "^##" $1 | head -2 | tail -1 | grep -E "^## Ingrédients \([0-9]+\)" | wc -l) != 1 ]]; then
    echo ERREUR: chapitre "Ingrédients" incorrect
  elif [[ $(grep "^##" $1 | head -3 | tail -1) != "## Recette" ]]; then
    echo ERREUR: chapitre "Recette" incorrect
  else
    echo OK
  fi
}

test_ingredients() {
  real_count=$(awk '/## Ingrédients/,/## Recette/' $1 | grep "^-" | wc -l)
  written_count=$(grep -e "## Ingrédients" $1 | sed -E "s#.*\((.*)\).*#\1#")

  if [[ $real_count != $written_count ]]; then
    echo ERREUR: incohérence sur le nombre d’ingrédients
  elif [[ $(awk '/## Ingrédients/,/## Recette/' $1 | grep "^-" | awk '{$1=$2=""; print $0}' | uniq -d | wc -l) != 0 ]]; then
    echo ERREUR: ingrédients dupliqués
  else
    echo OK
  fi
}

test_sommaire() {
  if [[ $(grep $1 sommaire.md | wc -l) == 0 ]]; then
    echo ERREUR: absent du sommaire
  elif [[ $(grep $1 sommaire.md | wc -l) > 1 ]]; then
    echo ERREUR: plusieurs fois dans le sommaire
  else
    echo OK
  fi
}

files=$(ls -1 *.md | grep -v sommaire.md)


for f in $files; do
  echo -e "\n\n== $f =="

  echo Sommaire............................$(test_sommaire $f)
  echo Titre...............................$(test_titre $f)
  echo Chapitres...........................$(test_chapitres $f)
  echo Ingrédients.........................$(test_ingredients $f)
done
