# Spice Cake

## Préparation

Temps total: 40 min

| Étape       | Durée |
| ------------| ----- |
| Préparation | 10mn  |
| Repos       | -     |
| Cuisson     | 30mn  |

## Ingrédients (5)

Pour 6 personnes :

- 200g chocolat noir
- 100g sucre en poudre
- 50g farine
- 100g beurre
- 3 œufs

## Recette

1. Préchauffez votre four à 180°C (thermostat 6). Dans une casserole, faites fondre le chocolat
et le beurre coupé en morceaux à feu très doux.

2. Dans un saladier, ajoutez le sucre, les oeufs, la farine. Mélangez.

3. Ajoutez le mélange chocolat/beurre. Mélangez bien.

4. Beurrez et farinez votre moule puis y versez la pâte à gâteau.

5. Faites cuire au four environ 20 minutes.

6. A la sortie du four le gâteau ne paraît pas assez cuit. C'est normal, laissez-le refroidir
puis démoulez- le.
